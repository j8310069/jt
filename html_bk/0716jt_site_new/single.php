<?php get_header(); ?>

	<div id="contents" class="clearfix">
		
		<div id="main" class="clearfix">

			<div class="single_content">
				<?php if(have_posts()):
					  while(have_posts()):
					  the_post();
				?>

				<!-- 記事タイトル部分 -->
				<section class="single_header">
					<section class="single_header_data clearfix">
						<p><time><?php the_time('Y.m.d'); ?></time></p>
						<ul class="clearfix">
							<?php $posttags = get_the_tags();
							if($posttags) {
								foreach($posttags as $tag) {
									echo '<li>' . $tag->name . '</li>';
								}
							}
							?>
						</ul>
						<h2><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></h2>
					</section>
					<section class="single_header_area">
						<h1><?php the_title(); ?></h1>
						<ul class="single_btn clearfix">
							<!-- fb -->
							<li class="fb">
								<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
							</li>
							<!-- twitter -->
							<li class="tw">
								<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" data-via="japanese_team">Tweet</a>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id; js.async = true; js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
							</li>
							<!-- はてブ -->
							<li class="hatena">
								<a href="http://b.hatena.ne.jp/entry/http://japanese-team.jp/" class="hatena-bookmark-button" data-hatena-bookmark-title="Japanese-TEAM" data-hatena-bookmark-layout="standard-balloon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="http://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
							</li>
							<!-- google -->
							<li class="google">
								<div class="g-plusone"></div>
								<script type="text/javascript">
								  window.___gcfg = {lang: 'ja'};

								  (function() {
								    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
								    po.src = 'https://apis.google.com/js/platform.js';
								    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
								  })();
								</script>
							</li>
						</ul>
					</section>
				</section>
				
				<section class="single_area">
					<?php the_content(); ?>
					<ul class="single_btn clearfix">
						<!-- fb -->
						<li class="fb">
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
						</li>
						<!-- twitter -->
						<li class="tw">
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" data-via="japanese_team">Tweet</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id; js.async = true; js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</li>
						<!-- はてブ -->
						<li class="hatena">
							<a href="http://b.hatena.ne.jp/entry/http://japanese-team.jp/" class="hatena-bookmark-button" data-hatena-bookmark-title="Japanese-TEAM" data-hatena-bookmark-layout="standard-balloon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="http://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
						</li>
						<!-- google -->
						<li class="google">
							<div class="g-plusone"></div>
							<script type="text/javascript">
							  window.___gcfg = {lang: 'ja'};

							  (function() {
							    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
							    po.src = 'https://apis.google.com/js/platform.js';
							    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
							  })();
							</script>
						</li>
					</ul>
					<p>ライター名:<?php the_author_nickname(); ?></p>
				</section>
				<section class="single_footer_area">
					<h4>関連記事</h4>
					<section class="single_footer_recommend clearfix">
						<?php
								$original_post = $post;
								$tags = wp_get_post_tags($post->ID);
								$tagIDs = array();
								if ($tags) {
								$tagcount = count($tags);
								for ($i = 0; $i < $tagcount; $i++) {
								$tagIDs[$i] = $tags[$i]->term_id;
								}

								$args=array(
									'tag__in' => $tagIDs,
									'post__not_in' => array($post->ID),
									'showposts'=>4,
									'caller_get_posts'=>1
								);

								$my_query = new WP_Query($args);
									if( $my_query->have_posts() ) {
									while ($my_query->have_posts()) : $my_query->the_post();
							?>

							<dl class="clearfix">
								<a href="<?php the_permalink();?>">
									<dt><?php the_post_thumbnail(array(95,95), array('alt'=>get_the_title(), 'title'=>get_the_title() )); ?></dt>
									<dd><?php if(mb_strlen($post->post_title)>35) { $title= mb_substr($post->post_title,0,35) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></dd>
								</a>
							</dl>
						
							<?php endwhile; wp_reset_query(); ?>
						<?php } else { ?>
							<p>関連する記事は見当たりません。</p>
						<?php } } ?>
					</section>

					<h4>この記事を書いた人</h4>
					<section class="single_footer_writter clearfix">
						<p><?php echo get_avatar(get_the_author_id(), 150); ?></p>
						<p><?php the_author_nickname(); ?><br /><?php the_author_description(); ?></p>
					</section>
					<section class="single_footer_comment">
						<div class="fb-comments" data-href="https://www.facebook.com/JapaneseTEAM.jp" data-width="100%" data-numposts="1" data-colorscheme="light"></div>
					</section>
				</section>	

			</div><!-- single_content -->

				
				<?php endwhile; ?>
			<?php endif; ?>
		</div><!-- #main -->
			
		<div id="top_sidebar"><?php get_sidebar(); ?></div>
			
	</div><!-- #contents -->

<?php get_footer();