<?php get_header(); ?>

<div id="contents" class="clearfix">
 
<!-- 検索ワードを出力 -->

	<div id="main" class="clearfix">
		<span class="page_theme">
			<p>検索結果：「<?php the_search_query(); ?>」</p>
 		</span>
		<?php $args = array(
			 	'posts_per_page'   => 18,
				'category'         => '1,2,3,4,5,6,7,8,9,10,11',
				'order'            => 'DESC' );
			$my_posts = get_posts($args);
			global $post; ?>
			<?php if(have_posts()): ?>
				<?php foreach($my_posts as $post) : setup_postdata($post); ?>
			 
				<?php get_template_part('loop', 'search'); ?>	
				<?php endforeach; ?>
			<?php else : ?>

			<p>「<span><?php the_search_query(); ?></span>」の検索結果が見つかりませんでした。</p>
			<p>別のキーワードでお試しください。</p>
			<!-- 検索フォームを表示-->
			<?php get_search_form(); ?>
		<?php endif;  ?>

	</div><!-- main -->
 	<div id="top_sidebar"><?php get_sidebar(); ?></div>
 </div><!-- #contents -->
<?php get_footer(); 
