// JavaScript Document
jQuery(function(){

// bxslider
    jQuery('.bxslider').bxSlider({
        auto: true,
        pause: 5000,
        speed: 1000,
    });

// アニメーションの時間
    var duration = 300;


// nav_animate
    jQuery('#navi ul li.nb:nth-child(n+2):nth-child(-n+6)')
        .on('mouseover', function(){
            jQuery(this).stop(true).animate({
                borderBottomWidth: '5px',
                height: '45px',
                backgroundColor: '#f5f5f5',
            }, duration, 'easeOutSine');
        })
        .on('mouseout', function(){
            jQuery(this).stop(true).animate({
                borderBottomWidth: '2px',
                height: '48px',
                backgroundColor: '#fff',
            }, duration, 'easeOutSine');
        });
// nav_dropdown
    jQuery( '#navi ul li:nth-child(6)' )
    	.on('mouseover', function() {
            jQuery( '#navi ul li:nth-child(6) ul.sub-menu' ).stop(true).slideDown(
                500,
                'swing');
        })
    	.on('mouseout', function(){
            jQuery( '#navi ul li:nth-child(6) ul.sub-menu' ).stop(true).slideUp(
                500,
                'swing');
    } );



//マウスオーバー時半透明  
    jQuery('article, #sidebar section.event_side ul li, .single_footer_recommend dl, .js_img li, .j_space_area img')
        .on('mouseover', function(){
            jQuery(this).stop(true).animate({
                opacity: '0.5'
            }, duration, 'easeOutSine');
        })
        .on('mouseout', function(){
            jQuery(this).stop(true).animate({
                opacity: '1'
            }, duration, 'easeOutSine');
        });
});

