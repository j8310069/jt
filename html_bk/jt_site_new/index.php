<?php get_header(); ?>

	<div id="contents" class="clearfix">
		<div class="headline">
			<ul class="bxslider">
			  <li><img src="<?php bloginfo('template_url'); ?>/img/J9.png" height="350" width="1000" alt="J-Space"/></li>
			  <li><img src="<?php bloginfo('template_url'); ?>/img/keyv_01.png" height="350" width="1000" alt="継承と創造"/></li>
			  <!-- <li><img src="<?php bloginfo('template_url'); ?>/img/top_images.png" height="350" width="1000" alt="継承と創造"/></li> -->
			</ul>
		</div>
		<div id="main" class="clearfix">

					<?php $args = array(
						 	'posts_per_page'   => 18,
							'category'         => '1,21,23,24,25,26,27,28,29,30,51,52',
							'order'            => 'DESC' );
						$my_posts = get_posts($args);
						global $post; ?>
					<?php if(have_posts()): ?>
						<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'index' ); ?>

						<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>

			
			<p class="more_side"><a href="<?php echo home_url( '/' ); ?>/all">もっとみる</a></p>
		
		</div><!-- main -->
			
			<div id="top_sidebar"><?php get_sidebar(); ?></div>
			
		</div><!-- #contents -->

<?php get_footer();