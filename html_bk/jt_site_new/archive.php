<?php get_header(); ?>

	<div id="contents" class="clearfix">

			<?php if(is_category('series')): ?>
	
				<p class="all green_border">連載一覧</p>
				<div id="main" class="clearfix">
					<?php
					$categories = get_categories('parent=30');
					foreach($categories as $category):
					?>
					<article class="category<?php $category_info = get_the_category(); echo $category_info[0]->cat_ID;?>">
						<a href="<?php echo get_category_link($category->cat_ID); ?>">
							
							<dl>
								<dt><?php echo $category->cat_name;?></dt>
								<dd><?php the_post_thumbnail( array(250, 167) ); ?></dd>
							</dl>
							<section class="post_data clearfix">
								<time>第<?php echo $category->count; ?>話掲載中</time>
									<ul class="clearfix">
										<?php $posttags = get_the_tags();
										if($posttags) {
											foreach($posttags as $tag) {
												echo '<li>' . $tag->name . '</li>';
											}
										}
										?>
									</ul>
							</section>
							<h3><?php if(mb_strlen($post->post_title)>29) { $title= mb_substr($post->post_title,0,29) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></h3>
							<p><?php the_author_nickname(); ?></p>
						</a>
					</article>
					<?php endforeach; ?>
				</div>				
				<!-- イベント一覧 -->
				<?php elseif(is_category('event')): ?>

				<p class="all blue_border">イベント一覧</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '21',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<!-- インタビュー一覧 -->
				<?php elseif(is_category('interview')): ?>

				<p class="all yellow_border">インタビュー記事一覧</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '1',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php endif; ?>
				</div>
				
				<!-- 歴史記事一覧 -->
				<?php elseif(is_category('history')): ?>

					<p class="all yellow_border">歴史記事一覧</p>
					<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '23',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
					</div>
				
				<!-- VIP一覧 -->

				<?php elseif(is_category('vip')): ?>

				<p class="all yellow_border">VIP一覧</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '51',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
				</div>
				<!-- 企画一覧 -->
				<?php elseif(is_category('id-6')): ?>

					<p class="all green_border">企画一覧</p>
					<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '24',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
					</div>

				<?php elseif(!is_category()): ?>
					<p class="categry<?php $category_info = get_the_category(); echo $category_info[0]->cat_ID;?>"><?php the_category(name); ?></p>
					<div id="main" class="clearfix">

						<?php if(have_posts()): ?>
						<?php while(have_posts()):
							  the_post();
						?>

							<?php get_template_part( 'loop', 'archive' ); ?>

						<?php endwhile; ?>
						<?php else : ?>
							<p>記事がありません</p>
						<?php endif; ?>
					</div>
				<?php endif; ?>

		<?php get_sidebar(); ?>
	</div>

	<?php get_footer();