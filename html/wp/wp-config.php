<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、言語、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'wp_01');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'root');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y1s7+h+B(Oh|[:>Vn45-<&|k%W+viEC3mFMI@OY|M2Am4o>Bm#N%QB-Wv6bsh~vW');
define('SECURE_AUTH_KEY',  'Z|b}uDV`+i5}mw>|fkGa_o_8q7<^W(c6+=@w;p6{Hn#n%cJ~nLLe;)+)k ^mc`k%');
define('LOGGED_IN_KEY',    'q/W&.Fl2r3_f=y0|e5Z}cl-1=:jLwHGO++6+fYZg/W3.-30 Ysw}~v-Da(R,+g-]');
define('NONCE_KEY',        'gG.A2&6/C.RW!A>zu|d-gXrx=<f89zvMT(+|(7%m/@CZGr5Y:B|Z#9^J|X%0TUfz');
define('AUTH_SALT',        'WQU@DoQu4T1oXNb-oWv69.ls8r7KE]Y-i@5H|+NZz+^f,T~g>--@zOCt.;1~i/TS');
define('SECURE_AUTH_SALT', 'tH]#W+{=%Q)^ptO5w|k{H!Bqsh0Lb$B2G*]SYw~4rz}<|&3]ckU$&S6bv@_y-h~T');
define('LOGGED_IN_SALT',   'by_-dK.[6`ohSA+yvMZ+}v0<|L=+DGOSMj>ho`yfWs>n[>IT=!7Y@<:uz?C{4EYD');
define('NONCE_SALT',       '#B*j|`9fE$!(8M4+Z.;tEBaM=R S2@+1~G|]PO&v}$XPE9-{Q1{P,zJ~BTvPS/b]');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_01';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
