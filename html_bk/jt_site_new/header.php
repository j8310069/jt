<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width ">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/jquery.bxslider.css">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=232536120281714&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="wrapper">
	<header id="header" class="clearfix">
		<section class="header_area clearfix">
			<h1><a href="<?php echo home_url( '/' ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo_01.png" height="80" width="200" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
			<h2><?php bloginfo( 'description' ); ?></h2>
		</section>
		<section class="utility">
			<ul class="clearfix">
				<li><a href="<?php echo home_url( '/' ); ?>/about/">社団概要</a></li>
				<li><a href="<?php echo home_url( '/' ); ?>/service/">寄与のお願い</a></li>
				<li><a href="<?php echo home_url( '/' ); ?>/form/">お問い合わせ</a></li>
				<li><a href="<?php echo home_url( '/' ); ?>/support/">協賛企業</a></li>
			</ul>

			<!-- 検索フォーム -->
			<?php get_search_form(); ?>
			
		</section>
	</header>

	<nav id="navi">
		<ul class="clearfix">
			<li>　</li>
			<li class="nb"><a href="<?php echo home_url( '/' ); ?>/">トップ</a></li>
			<li class="nb"><a href="<?php echo home_url( '/' ); ?>/all/">記事一覧</a></li>
			<li class="nb"><a href="<?php echo home_url( '/' ); ?>/archives/category/series/">連載一覧</a></li>
			<li class="nb"><a href="<?php echo home_url( '/' ); ?>/archives/category/event/">イベント</a></li>
			<li class="nb"><a href="#">カテゴリー</a>
				<ul class="sub-menu">
					<li><a href="<?php echo home_url( '/' ); ?>/archives/category/interview/">インタビュー</a></li>
					<li><a href="<?php echo home_url( '/' ); ?>/archives/category/id-6/">企画</a></li>
					<li><a href="<?php echo home_url( '/' ); ?>/archives/category/history/">歴史</a></li>
					<li><a href="<?php echo home_url( '/' ); ?>/archives/category/vip/">VIP</a></li>
				</ul>
			</li>
			<li>　</li>
		</ul>
	</nav>