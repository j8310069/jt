	<footer id="footer">
		<ul id="footerline">
			<li class="red"></li>
			<li class="yellow"></li>
			<li class="green"></li>
			<li class="blue"></li>
			<li class="black"></li>
		</ul>
		<p><img src="<?php bloginfo('template_url'); ?>/img/logo_01.png" height="76" width="170" alt=""></p>
		<ul class="text_navi clearfix">
			<li><a href="<?php echo home_url( '/' ); ?>/about/">社団概要</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>/service/">寄与のお願い</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>/form/">お問い合わせ</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>/support/">協賛企業</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>/privacy/">プライバシーポリシー</a></li>
		</ul>
		<ul class="text_navi_social clearfix">
			<li><a href="https://www.facebook.com/JapaneseTEAM.jp"><img src="<?php bloginfo('template_url'); ?>/img/btn_01_fb.png" height="20" width="20" alt="facebook"></a></li>
			<li><a href="https://twitter.com/japanese_team"><img src="<?php bloginfo('template_url'); ?>/img/btn_02_tw.png" height="20" width="20" alt="twitter"></a></li>
		</ul>
		<address>Copyright &copy; 若者法人 Japanese TEAM All Right Reserved.</address>
	</footer>

</div>
<?php wp_footer(); ?>
</body>
</html>
