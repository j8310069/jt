<?php get_header(); ?>
		
		<div id="contents" class="clearfix">

			<?php if(!is_page('all') && (!is_page('j-space'))): ?>
			
			<img src="<?php bloginfo('template_url'); ?>/img/page_images.png" alt="top">
			
			<div id="main" class="clearfix">

					<?php if(have_posts()): while(have_posts()): the_post();?>

						<?php the_title(); ?>
						<?php the_content(); ?>

					<?php endwhile; ?>
					<?php endif; ?>

			</div>

			<div id="top_sidebar"><?php get_sidebar(); ?></div>
			
			<!-- 記事一覧 -->
			<?php elseif(is_page(all)): ?>
			
			<p class="all yellow_border">記事一覧</p>

			<div id="main" class="clearfix">
				
					<?php $args = array(
							 	'posts_per_page'   => 18,
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'page' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>

			</div><!-- #main -->
			
			<?php get_sidebar(); ?>
			
			<!-- J-Space -->
			<?php elseif(is_page(j-space)): ?>
			
			<!-- <img src="<?php bloginfo('template_url'); ?>/img/J9.png" height="350" width="1000" alt="J-Space"/> -->
			
			<div id="main" class="clearfix">
				<div class="j_space">
				<?php if(have_posts()): while(have_posts()): the_post();?>
					<section class="single_header_area">
						<h1><?php the_title(); ?></h1>
						<ul class="single_btn clearfix">
							<!-- fb -->
							<li class="fb">
								<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
							</li>
							<!-- twitter -->
							<li class="tw">
								<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" data-via="japanese_team">Tweet</a>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id; js.async = true; js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
							</li>
							<!-- はてブ -->
							<li class="hatena">
								<a href="http://b.hatena.ne.jp/entry/http://japanese-team.jp/" class="hatena-bookmark-button" data-hatena-bookmark-title="Japanese-TEAM" data-hatena-bookmark-layout="standard-balloon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="http://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
							</li>
							<!-- google -->
							<li class="google">
								<div class="g-plusone"></div>
								<script type="text/javascript">
								  window.___gcfg = {lang: 'ja'};

								  (function() {
								    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
								    po.src = 'https://apis.google.com/js/platform.js';
								    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
								  })();
								</script>
							</li>
						</ul>
					</section>
					<section class="j_space_area">
						<div>
							<img src="<?php bloginfo('template_url'); ?>/img/img_01.png" height="280" width="650" alt="J-Space"/>
						</div>
						<ul class="js_img clearfix">
							<li><img src="<?php bloginfo('template_url'); ?>/img/img_02.png" height="180" width="320" alt="J-Space"></li>
							<li><img src="<?php bloginfo('template_url'); ?>/img/img_03.png" height="180" width="320" alt="J-Space"></li>
							<li><img src="<?php bloginfo('template_url'); ?>/img/img_04.png" height="180" width="320" alt="J-Space"></li>
							<li><img src="<?php bloginfo('template_url'); ?>/img/img_05.png" height="180" width="320" alt="J-Space"></li>
						</ul>
					</section>
					<section class="single_area">
						<?php the_content(); ?>

					</section>




				<?php endwhile; ?>
				<?php endif; ?>

				</div><!-- j-space -->
				
			</div><!-- #main -->
			
			<div id="top_sidebar"><?php get_sidebar(); ?></div>

			<?php endif; ?>

			
		</div><!-- #contents -->

<?php get_footer();