<?php get_header(); ?>

	<div id="contents" class="clearfix">

			<?php if(is_category('series')): ?>

				<p class="all green_border">連載一覧</p>
				<div id="main" class="clearfix">
					<?php
						$seriesArray = get_categories('parent=38');
						// 連載ページの子カテゴリを追加
						$seriesCategoryArray = array(39, 9);
						for( $seriesCount = 0; $seriesCount < count($seriesCategoryArray); $seriesCount++) :
			 				$seriesID = $seriesArray[$seriesCount]->cat_ID;
			 				$seriesName = $seriesArray[$seriesCount]->name;
			 				$seriesCategory = $seriesCategoryArray[$seriesCount];
				  ?>
					  <?php
		 				 	$args = array(
									 	'posts_per_page'   => 1,
									 	'category'         => $seriesCategory,
										'order'            => 'DESC' );
									$my_posts = get_posts($args);
									global $post;
					  ?>
						<?php if(have_posts()): ?>
							<?php foreach($my_posts as $post) : setup_postdata($post); ?>
								<article class="category<?php echo $seriesID;?>">
			            <a href="<?php echo get_category_link($seriesID); ?>">
			              <dl>
			                <dt><?php echo $seriesName;?></dt>
			                <dd><?php the_post_thumbnail( array(250, 167) ); ?></dd>
			              </dl>
			              <section class="post_data clearfix">
			                <time>第<?php echo $seriesArray[$seriesCount]->count; ?>話掲載中</time>
			                  <ul class="clearfix">
			                    <?php $posttags = get_the_tags();
			                    if($posttags) {
			                      foreach($posttags as $tag) {
			                        echo '<li>' . $tag->name . '</li>';
			                      }
			                    }
			                    ?>
			                  </ul>
			              </section>
			              <h3><?php if(mb_strlen($post->post_title)>29) { $title= mb_substr($post->post_title,0,29) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></h3>
			              <p><?php the_author_nickname(); ?></p>
			            </a>
			          </article>
							<?php endforeach; ?>
						<?php endif; ?>
				  <!-- for終了 -->
					<?php endfor; ?>
				  

				</div>
				<!-- イベント一覧 -->
				<?php elseif(is_category('event')): ?>

				<p class="all blue_border">イベント</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '21',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<!-- インタビュー一覧 -->
				<?php elseif(is_category('interview')): ?>

				<p class="all yellow_border">インタビュー</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '1',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php endif; ?>
				</div>

				<!-- オリンピック記事一覧 -->
				<?php elseif(is_category('history')): ?>

					<p class="all yellow_border">オリンピック</p>
					<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '23',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
					</div>

				<!-- スポーツ一覧 -->
				<?php elseif(is_category('sports')): ?>

				<p class="all yellow_border">スポーツ</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '65',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
				</div>
				<!-- アジア関連記事一覧 -->
				<?php elseif(is_category('ajia')): ?>

				<p class="all green_border">アジア大会</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '73',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
				</div>
				<!-- インフラ一覧 -->
				<?php elseif(is_category('infra')): ?>

				<p class="all green_border">インフラ</p>
				<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '77',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
				</div>
				<!-- 企画一覧 -->
				<?php elseif(is_category('id-6')): ?>

					<p class="all green_border">企画一覧</p>
					<div id="main" class="clearfix">

					<?php $args = array(
							 	'posts_per_page'   => 18,
							 	'category'         => '24',
								'order'            => 'DESC' );
							$my_posts = get_posts($args);
							global $post;
					?>

					<?php if(have_posts()): ?>
					<?php foreach($my_posts as $post) : setup_postdata($post); ?>

						<?php get_template_part( 'loop', 'archive' ); ?>

					<?php endforeach; ?>
					<?php else : ?>
						<p>記事がありません</p>
					<?php endif; ?>
					</div>

				<?php elseif(!is_category()): ?>
					<p class="categry<?php $category_info = get_the_category(); echo $category_info[0]->cat_ID;?>"><?php the_category(name); ?></p>
					<div id="main" class="clearfix">

						<?php if(have_posts()): ?>
						<?php while(have_posts()):
							  the_post();
						?>

							<?php get_template_part( 'loop', 'archive' ); ?>

						<?php endwhile; ?>
						<?php else : ?>
							<p>記事がありません</p>
						<?php endif; ?>
					</div>
				<?php endif; ?>

		<?php get_sidebar(); ?>
	</div>

	<?php get_footer();