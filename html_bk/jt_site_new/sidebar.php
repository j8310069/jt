		<aside id="sidebar">
			<section class="event_side">
				<h4>イベント</h4>
					<?php $args = array(
						 	'posts_per_page'   => 2,
						 	'category'		   => '21',
							'order'            => 'DESC' );
						$my_posts = get_posts($args);
						global $post; ?>
					<?php if(have_posts()): ?>
						<?php foreach($my_posts as $post) : setup_postdata($post); ?>
									<ul>
										<li class="category<?php $category_info = get_the_category(); echo $category_info[0]->cat_ID;?>">
											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large_thumbnails'); ?></a>
										</li>	
									</ul>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				<p class="more_side"><a href="<?php echo home_url( '/' ); ?>/archives/category/event/">もっとみる</a></p>
			</section>

			<section class="recommend_side">
				<h4>オススメの記事</h4>
					<?php $args = array(
						 	'posts_per_page'   => 5,
						 	'category'		   => '1,21,23,24,25,26,27,28,29,30,51',
							'order'            => 'DESC' );
						$my_posts = get_posts($args);
						global $post; ?>
					<?php if(have_posts()): ?>
						<?php foreach($my_posts as $post) : setup_postdata($post); ?>
									<article class="category<?php $category_info = get_the_category(); echo $category_info[0]->cat_ID;?>">
										<a href="<?php the_permalink(); ?>">
											<dl class="clearfix">
												<dt><?php the_post_thumbnail( array(58, 58) ); ?></dt>
												<dd><?php if(mb_strlen($post->post_title)>27) { $title= mb_substr($post->post_title,0,27) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></dd>
											</dl>
										</a>
									</article>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				<p class="more_side"><a href="<?php echo home_url( '/' ); ?>/all/">もっとみる</a></p>
			</section>
		<ul>
		<?php dynamic_sidebar( 'primary-widget-area' ); ?>
		</ul>
		</aside>
