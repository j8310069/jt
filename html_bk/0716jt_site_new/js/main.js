// JavaScript Document
jQuery(function(){

// bxslider
    jQuery('.bxslider').bxSlider({
        auto: true,
        pause: 5000,
        speed: 1000,
    });

// アニメーションの時間
    var duration = 300;


// nav_animate
    jQuery('#navi ul li.nb:nth-child(n+2):nth-child(-n+6)')
        .on('mouseover', function(){
            jQuery(this).stop(true).animate({
                borderBottomWidth: '5px',
                height: '45px',
                backgroundColor: '#f5f5f5',
            }, duration, 'easeOutSine');
        })
        .on('mouseout', function(){
            jQuery(this).stop(true).animate({
                borderBottomWidth: '2px',
                height: '48px',
                backgroundColor: '#fff',
            }, duration, 'easeOutSine');
        });
// nav_dropdown
    jQuery( '#navi ul li:nth-child(6)' )
    	.on('mouseover', function() {
            jQuery( '#navi ul li:nth-child(6) ul.sub-menu' ).stop(true).slideDown(
                500,
                'swing');
        })
    	.on('mouseout', function(){
            jQuery( '#navi ul li:nth-child(6) ul.sub-menu' ).stop(true).slideUp(
                500,
                'swing');
    } );



//マウスオーバー時半透明  
    jQuery('article, #sidebar section.event_side ul li, .single_footer_recommend dl, .js_img li, .j_space_area img')
        .on('mouseover', function(){
            jQuery(this).stop(true).animate({
                opacity: '0.5'
            }, duration, 'easeOutSine');
        })
        .on('mouseout', function(){
            jQuery(this).stop(true).animate({
                opacity: '1'
            }, duration, 'easeOutSine');
        });
});

// mobileではjsを切る
jQuery(document).ready(function($) {
    //PC環境の場合
    if (window.matchMedia( '(min-width: 400px)' ).matches) {
        $.ajax({
            url: 'main.js',
            dataType: 'script',
            cache: false
       });
    //モバイル環境の場合
    } else {
        $.ajax({
            url: '',
            dataType: 'script',
            cache: false
        });
    };
});

// ニュース欄

jQuery(function() {
var formatData = function(date) {
     var d = new Date(date);
     return (d.getFullYear() + '.' + ("0"+(d.getMonth()+1)).slice(-2)  + '.' + ("0"+d.getDate()).slice(-2));
};

    jQuery.getJSON(
         'http://ajax.googleapis.com/ajax/services/feed/load?callback=?',
         {
         q:'http://www.facebook.com/feeds/page.php?format=rss20&id=625813297488118',
         v:'1.0',
         num:3
         },
         function (data) {
         console.log(data.responseData.feed);
         jQuery.each(data.responseData.feed.entries, function(i, item){
            jQuery('.news ul').append('<li><span>'+ formatData(item.publishedDate) +'</span>' + 
                '<h4><a href="' + item.link + '">' + item.title + '</a></h4>' + '</li>');
            });
         }
    );
});
