<?php
/**
 * NCWhite site definitions
 */
if ( !function_exists( 'ncwhite_setup' ) ):
function ncwhite_setup() {

	/* Javascriptの読み込み */
	$template_url = get_bloginfo('template_directory');
	if( !is_admin() ){
 		if (( !function_exists(’is_mobile’)) || !is_mobile()) {
			wp_enqueue_script( 'jquery', "http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" );
			wp_enqueue_script( 'modernizr', "{$template_url}/modernizr.min.js" );
			wp_enqueue_script( 'jquery-ui', "{$template_url}/js/vendor/jquery-ui-1.10.3.custom.min.js" );
			wp_enqueue_script( 'bxslider', "{$template_url}/js/jquery.bxslider.min.js" );
			wp_enqueue_script( 'main', "{$template_url}/js/main.js" );
			wp_deregister_script( 'l10n' );
		} else {
  		/* for Mobile */	
		}
	}
	
	/* サムネールサポート */
	add_theme_support( 'post-thumbnails' );
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(250, 167 ,true);
	add_image_size('small_thumbnails',58,58,ture);
	add_image_size('large_thumbnails',190,127,ture);
	
	/* サイドバー */
	register_sidebar( array(
		'name' => 'Primary Widget Area',
		'id' => 'primary-widget-area',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

} // end of function ncwhite_setup()
endif;


/**
 * NCWhite title filtering
 */
if ( !function_exists( 'ncwhite_titlefiltering' ) ):
function ncwhite_titlefiltering() {

	/* wp_titleのフィルタリング */
	function custom_filter_wp_title( $title, $separator ) {
		// Don't affect wp_title() calls in feeds.
		if ( is_feed() )
			return $title;
	
		global $paged, $page;
	
		if ( is_search() ) {
			// If we're a search, let's start over:
			$title = get_search_query() . "の検索結果";
			// Add a page number if we're on page 2 or more:
			if ( $paged >= 2 )
				$title .= " (" . $paged .")";
			// Add the site name to the end:
			$title .= " $separator " . get_bloginfo( 'name', 'display' );
			// We're done. Let's send the new title back to wp_title():
			return $title;
		}
		
		// Otherwise, let's start by adding the site name to the end:
		$title .= get_bloginfo( 'name', 'display' );
	
		// If we have a site description and we're on the home/front page, add the description:
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $separator " . $site_description;
		}
	
		// Add a page number if necessary:
		if ( $paged >= 2 ) {
				$title = "(" . $paged .") " . $title;
		}
		if ( $page >= 2 ) {
				$title = "(" . $page .") " . $title;
		}
	
		// Return the new title to wp_title():
		return $title;
	}
	add_filter( 'wp_title', 'custom_filter_wp_title', 10, 2 );

} // end of function ncwhite_titlefiltering()
endif;


// Add Actions
add_action( 'after_setup_theme', 'ncwhite_setup' );
add_action( 'after_setup_theme', 'ncwhite_titlefiltering' );

// Add Editor Style
add_editor_style('editor-style.css');


/*
 * NCWhite default functions
 */

// トップレベルの先祖ページを探す
function get_toplevel_ancestor_page_id( $post ) {
	if ( $post->post_parent )	{
		$ancestors = get_post_ancestors( $post->ID );
		$root = count ( $ancestors ) - 1;
		$parent_id = $ancestors[$root];
	} else {
		$parent_id = $post->ID;
	}
	return $parent_id;
}
