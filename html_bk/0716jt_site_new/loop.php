	<article class="category<?php $category_info = get_the_category(); echo $category_info[0]->cat_ID;?>">
		<a href="<?php the_permalink(); ?>">
			<dl>
				<dt><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></dt>
				<dd><?php the_post_thumbnail( array(250, 167) ); ?></dd>
			</dl>
			<section class="post_data clearfix">
				<time><?php the_time('Y.m.d'); ?></time>
					<ul class="clearfix">
						<?php $posttags = get_the_tags();
						if($posttags) {
							foreach($posttags as $tag) {
								echo '<li>' . $tag->name . '</li>';
							}
						}
						?>
					</ul>
			</section>
			<h3><?php if(mb_strlen($post->post_title)>29) { $title= mb_substr($post->post_title,0,29) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></h3>
			<p><?php the_author_nickname(); ?></p>
		</a>
	</article>
	